package com.example.hp.chartdrawer;

import android.graphics.Color;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.androidplot.xy.BoundaryMode;
import com.androidplot.xy.LineAndPointFormatter;
import com.androidplot.xy.SimpleXYSeries;
import com.androidplot.xy.XYGraphWidget;
import com.androidplot.xy.XYPlot;
import com.androidplot.xy.XYSeries;

import java.text.FieldPosition;
import java.text.Format;
import java.text.ParsePosition;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity {
    final static String TAG = MainActivity.class.getSimpleName();
    private XYPlot plot;
    double buffer[] = new double[50];
    LineAndPointFormatter seriesFormat;
    int bufferDataLength = 0;
    private Handler h;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        h = new Handler();
        plotInit();

        Thread dataGenerateThread = new Thread(new Runnable() {
            @Override
            public void run() {
                synchronized (this) {
                    while (true) {
//                        bufferDataLength = new Random().nextInt(20) + 10;
                        for (int i = 0; i < buffer.length; i++) {
                            buffer[i] = new Random().nextDouble() * 3;
                        }

                        try {
                            TimeUnit.MILLISECONDS.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        });

        Thread drawningThread = new Thread(new Runnable() {
            @Override
            public void run() {
                synchronized (this) {
                    while (true) {
                        List<Number> list = new ArrayList<>();
                        for (int i = 0; i < buffer.length; i++) {
                            list.add(buffer[i]);
                        }

                        XYSeries series = new SimpleXYSeries(list, SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, "Series1");

                        seriesFormat = new LineAndPointFormatter(Color.RED, Color.GREEN, Color.GRAY, null);

                        plot.addSeries(series, seriesFormat);
                        plot.redraw();
                        try {
                            TimeUnit.MILLISECONDS.sleep(500);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        plot.clear();
                    }
                }
            }
        });

        dataGenerateThread.start();
        drawningThread.start();
    }

    private void plotInit() {
        plot = (XYPlot) findViewById(R.id.plot);

        plot.setRangeBoundaries(-200, 200, BoundaryMode.FIXED);
        plot.setRangeUpperBoundary(4, BoundaryMode.FIXED);
        plot.setRangeLowerBoundary(-4, BoundaryMode.FIXED);

        plot.getGraph().getLineLabelStyle(XYGraphWidget.Edge.BOTTOM).setFormat(new Format() {
            @Override
            public StringBuffer format(Object obj, StringBuffer toAppendTo, FieldPosition pos) {
                int i = Math.round(((Number) obj).floatValue());
                return toAppendTo.append(i);
            }

            @Override
            public Object parseObject(String source, ParsePosition pos) {
                return null;
            }
        });
    }
}
